insert into client_workflow values (1,'fdb001','textEn001',2);
insert into client_workflow values (2,'fdb001','taxEn001',1);

insert into client_configs values (1, 'fdb001','fashionDB','http://qa-crawlers:7005/myntra-fdb-service/fdb/product/enriched');

insert into enrichers values ('textEn001','http://10.148.87.46:7001/AttrTags','textEnricher','rest');

insert into enrichers values ('imgEn001','http://122.248.19.238:8080/enrich','imageEnricher','rest');

insert into enricher_attr values (1, 'textEn001','text_to_enrich','desc');
insert into enricher_attr values (2, 'imgEn001','colour','color');
insert into enricher_attr values (3, 'taxEn001','ALL','ALL');

 insert into enrichers values ('taxEn001','http://10.148.87.46:7002/TaxonomyTransformationv2/mapify/mapper/post','normalized','rest');
