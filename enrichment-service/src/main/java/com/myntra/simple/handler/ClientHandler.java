package com.myntra.simple.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.myntra.commons.exception.ManagerException;
import com.myntra.simple.client.entry.EnrichedProductEntry;
import com.myntra.simple.client.entry.EnricherEntry;
import com.myntra.simple.client.entry.RequestObjectEntry;
import com.myntra.simple.client.response.EnricherResponse;
import com.myntra.simple.entity.ClientConfigEntity;
import com.myntra.simple.entity.ClientWorkflowEntity;
import com.myntra.simple.manager.ClientConfigManager;
import com.myntra.simple.manager.ClientWorkflowManager;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public class ClientHandler {

    private static final Logger LOGGER = Logger.getLogger(ClientHandler.class);

    @Autowired
    private ClientWorkflowManager clientWorkflowManager;

    @Autowired
    private ClientConfigManager clientConfigManager;

    @Autowired
    private EnricherHandler enricherHandler;

    Map<String,List<ClientWorkflowEntity>> clientWorkflow=new HashMap<>();
    Map<String,ClientConfigEntity> clientConfig=new HashMap<>();
    


    private List<ClientWorkflowEntity> getWorkflow(String clientId){
        LOGGER.info("Getting Workflow for clientID : "+clientId);
        if(clientWorkflowManager == null){
            LOGGER.error("clientWorkflowManager is null");
        }
        if(clientWorkflow.get(clientId)==null) {
            List<ClientWorkflowEntity> workflow = null;
            try {
                workflow = clientWorkflowManager.getWorkflow(clientId);

            } catch (ManagerException e) {
                e.printStackTrace();
            }
            Collections.sort(workflow);
            clientWorkflow.put(clientId,workflow);
        }
        return clientWorkflow.get(clientId);
    }
    
    private ClientConfigEntity getClientConfig(String clientId){
        LOGGER.info("Getting The Response URL for clientID : "+clientId);
        if(clientConfig.get(clientId)==null) {
            ClientConfigEntity config = null;
            try {
            	config = clientConfigManager.getConfig(clientId);
            } catch (ManagerException e) {
                e.printStackTrace();
            }
            clientConfig.put(clientId,config);
        }
        return clientConfig.get(clientId);
    }


    public void enrich(RequestObjectEntry requestObjectEntry,JsonNode entity, String clientId){
        EnrichedProductEntry enrichedProductEntry = new EnrichedProductEntry();
        List<EnricherEntry> enricherResponseList = new ArrayList<>();
        for(ClientWorkflowEntity clientWorkflowEntity : getWorkflow(clientId)){
            LOGGER.info("Picking one Entity from Workflow and calling EnrichHandler.enrich() from ClientHandler");
            try{
            	EnricherResponse enricherResponse = enricherHandler.enrich(requestObjectEntry.getId(),entity, clientWorkflowEntity.getEnricherID());
            	enricherResponseList.add(new EnricherEntry(enricherResponse.getName(),enricherResponse.getVersion(), enricherResponse.getResponse().getResponse()));
            }catch (Exception e){
            	LOGGER.info("Some error occured while calling enricher:"+clientWorkflowEntity.getEnricherID()+". Skipping the enricher");
            	LOGGER.error( e.getMessage(),e);
            }

        }
        enrichedProductEntry.setEnricherResponseList(enricherResponseList);
        enrichedProductEntry.setSource(requestObjectEntry.getData().getEntity());
        enrichedProductEntry.setId(requestObjectEntry.getId());
       
        LOGGER.info("Enriched Response from all enrichers for Id "+requestObjectEntry.getId()+" is " + enrichedProductEntry.toString());
      //  enrichedBuffer.append("}");
       // LOGGER.info("Enriched Data is :" +enrichedBuffer.toString());
        handleResponse(enrichedProductEntry, clientId, requestObjectEntry.getId());
    }
    
    private void handleResponse(EnrichedProductEntry enrichedProductEntry, String clientId, String Id){
        LOGGER.info("Handling Enrichment Response for clientId :" + clientId+ " and Id : "+Id);
    	Map<String,String> headers=new HashMap<>();
        headers.put("Content-Type","application/json");
        headers.put("Accept","application/json");
        headers.put("Authorization","Basic YTpi");
        HttpHandler httpHandler=new HttpHandler();
        LOGGER.info("Sending Response back to client :"+clientId+". Response is "+enrichedProductEntry.toString());
    	httpHandler.post(enrichedProductEntry, headers, getClientConfig(clientId).getResponseUrl());
    }

}
