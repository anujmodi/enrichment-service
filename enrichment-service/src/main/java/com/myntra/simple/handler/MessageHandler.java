package com.myntra.simple.handler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.simple.client.entry.RequestObjectEntry;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public class MessageHandler {

    private static final Logger LOGGER = Logger.getLogger(MessageHandler.class);


    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ClientHandler clientHandler;


    public void handleMessage(RequestObjectEntry requestObjectEntry){

        String clientId = requestObjectEntry.getData().getClientId();
        JsonNode entity = requestObjectEntry.getData().getEntity();
        LOGGER.info("Message Handler got a Message from Kafka with id : "+requestObjectEntry.getId()+" and clientId : "+clientId);
        LOGGER.info("Payload is "+entity);
        LOGGER.info("Sending Message to ClientHandler");
        clientHandler.enrich(requestObjectEntry,entity, clientId);
    }



    public String getClientId(String data){
        try{
            JsonNode root = objectMapper.readTree(data);
            String clientId = root.path("clientId").toString();
            LOGGER.info("MessageHandler: Client Id : "+clientId);
            return clientId;
        }catch (Exception e){
            LOGGER.error("MessageHandler : Exception : getClientID(). Message : "+e.getMessage() );
            return null;
        }
    }


    public String getPayload(String data){

        try{
            JsonNode root = objectMapper.readTree(data);
            String payload = root.path("entity").toString();
            return payload;
        }catch (Exception e){
            LOGGER.error("Exception : getPayload(). Message : "+e.getMessage() );
            return null;
        }

    }


}
