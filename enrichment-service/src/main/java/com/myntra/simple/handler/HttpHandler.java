package com.myntra.simple.handler;

import java.util.Map;

import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.Context;
import com.myntra.commons.utils.ServiceURLProperty;
import com.myntra.simple.client.entry.EnricherRequestEntry;
import com.myntra.simple.client.entry.EnrichedProductEntry;
import com.myntra.simple.client.response.EnrichedProductResponse;
import com.myntra.simple.client.response.EnricherResponse;
import org.apache.log4j.Logger;

/**
 * Created by Riya Agarwal on 30/10/15.
 */

public class HttpHandler {
	private static final Logger LOGGER = Logger.getLogger(HttpHandler.class);


	public EnricherResponse post(EnricherRequestEntry enricherRequestEntry, Map<String, String> headers, String serviceUrl)  {
        LOGGER.info(enricherRequestEntry.toString());
		LOGGER.info("Trying to hit URL:" + serviceUrl + " Payload=" + enricherRequestEntry.toString() + " , Headers=" + headers);
		
		BaseWebClient client = null;
		try {
			client = new BaseWebClient(serviceUrl, ServiceURLProperty.MFS_URL, "application/json","application/json",Context.getContextInfo());
		} catch (ERPServiceException e) {
			e.printStackTrace();
		}
		for(Map.Entry<String,String> entry : headers.entrySet()) {
			client.setHeader(entry.getKey(), entry.getValue());
		}
		try {
			EnricherResponse enricherResponse =  client.post(enricherRequestEntry, EnricherResponse.class);
            LOGGER.info("Enricher Info :"+enricherResponse.toString());
			return enricherResponse;
		} catch (ERPServiceException e) {
			LOGGER.fatal("ERPServiceException in HttpHandler");
			return null;
		}
	}

    public void post(EnrichedProductEntry enrichedProductEntry, Map<String, String> headers, String serviceUrl)  {
        LOGGER.info("SENDING RESPONSE BACK : "+enrichedProductEntry.getId() +" to client URL:" + serviceUrl + " Payload = " + enrichedProductEntry.toString() + " , Headers=" + headers);
      
        BaseWebClient client = null;
        try {
            client = new BaseWebClient(serviceUrl, ServiceURLProperty.MFS_URL,"application/json","application/json", Context.getContextInfo());
        } catch (ERPServiceException e) {
            e.printStackTrace();
        }
        for(Map.Entry<String,String> entry : headers.entrySet()) {
            client.setHeader(entry.getKey(), entry.getValue());
        }
        try {
             client.put(enrichedProductEntry, EnrichedProductResponse.class);
            //return enricherResponse;
        } catch (ERPServiceException e) {
            LOGGER.fatal("ERPServiceException in HttpHandler");
          //  return null;
        }
    }
}
