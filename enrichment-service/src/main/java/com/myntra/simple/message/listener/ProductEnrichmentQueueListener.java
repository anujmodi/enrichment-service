package com.myntra.simple.message.listener;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.simple.client.entry.RequestObjectData;
import com.myntra.simple.client.entry.RequestObjectEntry;
import com.myntra.simple.handler.MessageHandler;

public class ProductEnrichmentQueueListener {
	private static final Logger LOGGER = Logger.getLogger(ProductEnrichmentQueueListener.class);

	@Autowired
	private MessageHandler messageHandler;

	@Autowired
	private ObjectMapper objectMapper;

	public void processMessage(Map<String, Map<Integer, List<byte[]>>> msgs) {

		for (Map.Entry<String, Map<Integer, List<byte[]>>> entry : msgs.entrySet()) {
			ConcurrentHashMap<Integer, List<byte[]>> messages = (ConcurrentHashMap<Integer, List<byte[]>>) entry
					.getValue();
			Collection<List<byte[]>> values = messages.values();
			for (Iterator<List<byte[]>> iterator = values.iterator(); iterator.hasNext();) {
				List<byte[]> list = iterator.next();
				for (byte[] object : list) {
					String rootJson = new String(object);
					try {
						RequestObjectEntry requestObjectEntry = getRequestObjectEntry(rootJson);
						LOGGER.info("Got Message from Kafka with id : " +requestObjectEntry.getId()+", clientId : "+requestObjectEntry.getData().getClientId()+" and Entity : "+requestObjectEntry.getData().getEntity());
						messageHandler.handleMessage(requestObjectEntry);
					} catch (JsonParseException e) {
						System.out.println("invalid product json");
						LOGGER.error(String.format("Invalid product json \n %s", rootJson));
					} catch (Exception e) {
						LOGGER.error(String.format("Failed to process message %s", rootJson), e);
					}
				}
			}
		}
	}

	private RequestObjectEntry getRequestObjectEntry(String rootJson) throws IOException {
		JsonNode sourceNode = objectMapper.readTree(rootJson);
		String id = sourceNode.path("id").toString();
		JsonNode jsonNode = sourceNode.path("data");
		RequestObjectData data = objectMapper.treeToValue(jsonNode, RequestObjectData.class);
		return new RequestObjectEntry(id, data);
	}

}