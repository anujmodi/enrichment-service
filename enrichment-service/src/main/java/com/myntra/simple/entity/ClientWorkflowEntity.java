package com.myntra.simple.entity;

import com.myntra.commons.entities.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
@Entity
@Table(name = "client_workflow")
public class ClientWorkflowEntity implements Comparable, AbstractEntity {
	@Id
	@GeneratedValue
	@Column(name = "id")
	String id;

	@Column(name = "client_id")
	String clientId;

	@Column(name = "enricher_id")
	String enricherID;


	@Column(name = "flow_order")
	int order;

	public ClientWorkflowEntity() {
		// TODO Auto-generated constructor stub
	}

	public ClientWorkflowEntity(String clientId,  String enricherID, String enricher_name,int order) {
		this.clientId = clientId;
		this.enricherID = enricherID;
		this.order = order;
	}

	@Override
	public String getId() {
		return clientId;
	}

	public void setId(String clientId) {
		this.clientId = clientId;
	}


	public String getEnricherID() {
		return enricherID;
	}

	public void setEnricherID(String enricherID) {
		this.enricherID = enricherID;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}


	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "ClientWorkflowEntity{" +
				"id='" + id + '\'' +
				", clientId='" + clientId + '\'' +
				", enricherID='" + enricherID + '\'' +
				", order=" + order +
				'}';
	}

	@Override
	public int compareTo(Object o) {
		int compareOrder = ((ClientWorkflowEntity) o).getOrder();
		return this.order - compareOrder;
	}
}
