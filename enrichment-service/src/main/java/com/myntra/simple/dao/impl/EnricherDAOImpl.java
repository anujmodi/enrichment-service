package com.myntra.simple.dao.impl;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.simple.dao.EnricherDAO;
import com.myntra.simple.entity.EnricherEntity;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public class EnricherDAOImpl extends BaseDAOImpl<EnricherEntity> implements EnricherDAO {
}
