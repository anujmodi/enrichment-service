package com.myntra.simple.dao;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.simple.entity.EnricherEntity;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public interface EnricherDAO extends BaseDAO<EnricherEntity> {
}
