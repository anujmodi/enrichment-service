package com.myntra.simple.dao.impl;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.simple.dao.ClientWorkflowDAO;
import com.myntra.simple.entity.ClientWorkflowEntity;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public class ClientWorkflowDAOImpl extends BaseDAOImpl<ClientWorkflowEntity> implements ClientWorkflowDAO {
}
