package com.myntra.simple.service.impl;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.ApplicationPropertyUtil;
import com.myntra.simple.client.codes.SimpleErrorCodes;
import com.myntra.simple.client.codes.SimpleSuccessCodes;
import com.myntra.simple.client.entry.GreetingEntry;
import com.myntra.simple.client.response.GreetingResponse;
import com.myntra.simple.service.GreetingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pavankumar.at on 27/07/15.
 */
public class GreetingServiceImpl implements GreetingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GreetingServiceImpl.class.getName());

    @Override
    public GreetingResponse getGreeted(String name) {

        GreetingResponse greetingResponse = new GreetingResponse(new StatusResponse(SimpleErrorCodes.GENERIC_ERROR, StatusResponse.Type.ERROR));
        List<GreetingEntry> greetingEntryList = new ArrayList<>();

        try {
            String greeting = ApplicationPropertyUtil.getStringAppProperty("Hello","","") + " " + name;
            GreetingEntry greetingEntry = new GreetingEntry(greeting);
            greetingEntryList.add(greetingEntry);
        } catch (ERPServiceException e) {
            LOGGER.info("Error getting a greeting", e);
            return greetingResponse;
        }
        greetingResponse.setStatus(new StatusResponse(SimpleSuccessCodes.SUCCESS, StatusResponse.Type.SUCCESS));
        greetingResponse.setData(greetingEntryList);
        return greetingResponse;
    }
}
