package com.myntra.simple.manager;

import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.simple.client.entry.RequestObjectEntry;
import com.myntra.simple.entity.ClientWorkflowEntity;

import java.util.List;

/**
 * Created by Aditya Upadhyaya on 26/10/15.
 */

public interface ClientWorkflowManager extends BaseManager<RequestObjectEntry,ClientWorkflowEntity>{

        List<ClientWorkflowEntity> getWorkflow( String clientID) throws ManagerException;
}
