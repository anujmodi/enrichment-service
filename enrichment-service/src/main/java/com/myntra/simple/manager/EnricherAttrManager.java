package com.myntra.simple.manager;

import com.myntra.commons.manager.BaseManager;
import com.myntra.simple.client.entry.EnricherAttrEntry;
import com.myntra.simple.entity.EnricherAttrEntity;

import java.util.List;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public interface EnricherAttrManager extends BaseManager<EnricherAttrEntry,EnricherAttrEntity>{

    public List<EnricherAttrEntity> getEnricherAttrEntities(String enricherID);
}
