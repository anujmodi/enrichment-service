package com.myntra.simple.manager;

import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.simple.client.entry.RequestObjectEntry;
import com.myntra.simple.entity.ClientConfigEntity;

/**
 * Created by Aditya Upadhyaya on 26/10/15.
 */

public interface ClientConfigManager extends BaseManager<RequestObjectEntry,ClientConfigEntity>{

        ClientConfigEntity getConfig( String clientID) throws ManagerException;
}
