package com.myntra.simple.manager.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.simple.client.entry.EnricherAttrEntry;
import com.myntra.simple.entity.EnricherAttrEntity;
import com.myntra.simple.manager.EnricherAttrManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public class EnricherAttrManagerImpl extends BaseManagerImpl<EnricherAttrEntry,EnricherAttrEntity> implements EnricherAttrManager {


    @Autowired
    ObjectMapper objectMapper;

    public List<EnricherAttrEntity> getEnricherAttrEntities(String enricherID){

        List<EnricherAttrEntity> list = dao.getAllObjectsForQuery("SELECT * FROM enricher_attr WHERE enricher_id = '"+enricherID+"';");

        return list;

    }

}
