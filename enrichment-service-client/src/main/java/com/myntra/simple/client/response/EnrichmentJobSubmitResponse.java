package com.myntra.simple.client.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;

/**
 * Created by Aditya Upadhyaya on 26/10/15.
 */

@XmlRootElement(name = "enrichmentJobSubmitResponse")
public class EnrichmentJobSubmitResponse extends AbstractResponse {

    @XmlElement(name = "clientId")
    private String clientId;

    @XmlElement(name = "data")
    private String data;


    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "\"clientId\":\"" + clientId + "\"" +
                ",\"data\":" + data +
                "}";
    }
}
