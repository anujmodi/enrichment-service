package com.myntra.simple.client.entry;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myntra.commons.entries.AbstractEntry;


@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize()
public class EnrichedProductEntry extends AbstractEntry {

	public EnrichedProductEntry() {
		// TODO Auto-generated constructor stub
	}
	
	@JsonProperty("id")
	String id;
	
    @JsonProperty("source")
    JsonNode source;

    @JsonProperty("enrichers")
    List<EnricherEntry> enricherEntries;

    public JsonNode getSource() {
        return source;
    }

    public void setSource(JsonNode source) {
        this.source = source;
    }

    public List<EnricherEntry> getEnricherResponseList() {
        return enricherEntries;
    }
    

    /**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	public void setEnricherResponseList(List<EnricherEntry> enricherResponseList) {
        this.enricherEntries = enricherResponseList;
    }

    @Override
    public String toString() {
        return "{" +
        		  "\"id\":" + getId()  +
                ",\"source\":" + getSource()  +
                ",\"enrichers\":" + getEnricherResponseList() +
                "}";
    }
}
