package com.myntra.simple.client.entry;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.myntra.commons.entries.AbstractEntry;


public class RequestObjectEntry extends AbstractEntry {

    @JsonProperty("id")
    private String id;

    @JsonProperty("data")
    private RequestObjectData data;

    public RequestObjectEntry() {
		// TODO Auto-generated constructor stub
	}
    
    public RequestObjectEntry(String id, RequestObjectData data) {
        this.id = id;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RequestObjectData getData() {
        return data;
    }

    public void setData(RequestObjectData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id  +
                ",\"data\":" + data +
                "}";
    }
}