package com.myntra.simple.client.codes;

import com.myntra.commons.codes.ERPSuccessCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public class SimpleSuccessCodes extends ERPSuccessCodes {

    private static final String BUNDLE_NAME = "SimpleSuccessCodes";

    public SimpleSuccessCodes(int code, String message) {
        setAll(code, message, BUNDLE_NAME);
    }

    public static final StatusCodes ORDER_FETCHED_SUCCESSFULLY = new SimpleSuccessCodes(2001, "ORDER_FETCHED_SUCCESSFULLY");
    public static final StatusCodes ORDER_ALREADY_IN_STATUS = new SimpleSuccessCodes(2002, "ORDER_ALREADY_IN_STATUS");
    public static final StatusCodes ORDER_UPDATED_SUCCESSFULLY = new SimpleSuccessCodes(2003, "ORDER_UPDATED_SUCCESSFULLY");
    public static final StatusCodes ORDER_ADDED_SUCCESSFULLY = new SimpleSuccessCodes(2004, "ORDER_ADDED_SUCCESSFULLY");

    public static final StatusCodes ORDER_ITEM_FETCHED_SUCCESSFULLY = new SimpleSuccessCodes(2005, "ORDER_ITEM_FETCHED_SUCCESSFULLY");
    public static final StatusCodes ORDER_ITEM_ALREADY_IN_STATUS = new SimpleSuccessCodes(2006, "ORDER_ITEM_ALREADY_IN_STATUS");
    public static final StatusCodes ORDER_ITEM_UPDATED_SUCCESSFULLY = new SimpleSuccessCodes(2007, "ORDER_ITEM_UPDATED_SUCCESSFULLY");
    public static final StatusCodes ORDER_ITEM_ADDED_SUCCESSFULLY = new SimpleSuccessCodes(2008, "ORDER_ITEM_ADDED_SUCCESSFULLY");

    public static final StatusCodes ORDER_CANCELLED_SUCCESSFULLY = new SimpleSuccessCodes(2009, "ORDER_CANCELLED_SUCCESSFULLY");
    public static final StatusCodes ORDER_DECLINED_SUCCESSFULLY = new SimpleSuccessCodes(2010, "ORDER_DECLINED_SUCCESSFULLY");

}
