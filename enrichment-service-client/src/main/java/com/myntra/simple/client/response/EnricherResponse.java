package com.myntra.simple.client.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myntra.commons.response.AbstractResponse;

/**
 * Created by Aditya Upadhyaya on 02/11/15.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize()
public class EnricherResponse extends AbstractResponse {

	@JsonProperty("name")
	String name;

	@JsonProperty("version")
	String version;

	@JsonProperty("response")
	EnricherResponseData response;
	public EnricherResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public EnricherResponseData getResponse() {
		return response;
	}

	public void setResponse(EnricherResponseData response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "{" + "\"status\":" + getStatus() + ",\"version\":" + getVersion()+ ",\"name\":" + getName() + ",\"response\":" + getResponse()
				+ "}";
	}
}
