package com.myntra.simple.client.entry;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myntra.commons.entries.AbstractEntry;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize()
public class EnricherEntry extends AbstractEntry {

	@JsonProperty("name")
	String name;

	@JsonProperty("version")
	String version;

	@JsonProperty("response")
	JsonNode response;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public JsonNode getResponse() {
		return response;
	}

	public void setResponse(JsonNode response) {
		this.response = response;
	}
	
	public EnricherEntry() {
		// TODO Auto-generated constructor stub
	}
	
	

	public EnricherEntry(String name, String version, JsonNode response) {
		this.name = name;
		this.version = version;
		this.response = response;
	}

	@Override
	public String toString() {
		return "{" +  "\"version\":" + getVersion()+ ",\"name\":" + "\""+getName() +"\""+ ",\"response\":" + getResponse()
				+ "}";
	}
}
