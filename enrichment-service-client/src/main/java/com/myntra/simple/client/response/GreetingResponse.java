package com.myntra.simple.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.simple.client.entry.GreetingEntry;

/**
 * Created by pavankumar.at on 27/07/15.
 */
@XmlRootElement(name = "greetingResponse")
/**
 * This is how a custom Response is written
 */
public class GreetingResponse extends AbstractResponse {

    private List<GreetingEntry> greeting;

    @XmlElementWrapper(name = "data")
    @XmlElement(name = "greeting")
    public List<GreetingEntry> getData() {
        return greeting;
    }

    public final void setData(List<GreetingEntry> greeting) {
        this.greeting = greeting;
    }

    public GreetingResponse() {}

    public GreetingResponse(StatusResponse statusResponse) {
        super(statusResponse);
    }

    public GreetingResponse(List<GreetingEntry> orderEntry) {
        super();
        setData(orderEntry);
    }

    @Override
    public String toString() {
        return "GreetingResponse{" +
                "greeting=" + greeting +
                '}';
    }
}
