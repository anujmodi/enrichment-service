package com.myntra.simple.client.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myntra.commons.response.AbstractResponse;

/**
 * Created by Aditya Upadhyaya on 02/11/15.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize()
public class EnricherResponseData extends AbstractResponse {

	@JsonProperty("id")
	String id;


	@JsonProperty("data")
	JsonNode data;

	

	public JsonNode getResponse() {
		return data;
	}

	public void setResponse(JsonNode response) {
		this.data = response;
	}

	@Override
	public String toString() {
		return "{" + ",\"id\":" + getId() + ",\"data\":" + getResponse()
				+ "}";
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
}
