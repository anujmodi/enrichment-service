package com.myntra.simple.client.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.databind.JsonNode;
import com.myntra.commons.entries.AbstractEntry;

/**
 * Created by Aditya Upadhyaya on 26/10/15.
 */
@XmlRootElement(name = "enrichmentRequest")
public class EnrichProductRequest extends AbstractEntry {

    @XmlElement(name = "clientId")
    private String clientId;

    @XmlElement(name = "data")
    private JsonNode data;

    public EnrichProductRequest(String clientId, JsonNode data) {
        this.clientId = clientId;
        this.data = data;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public JsonNode getData() {
        return data;
    }

    public void setData(JsonNode data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "\"clientId\":\"" + clientId + "\"" +
                ",\"data\":" + data +
                "}";
    }
}
