package com.myntra.simple.client.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myntra.commons.response.AbstractResponse;

/**
 * Created by Aditya Upadhyaya on 02/11/15.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize()
public class EnrichedProductResponse extends AbstractResponse {

	 
	 public EnrichedProductResponse() {
		// TODO Auto-generated constructor stub
	}

//    @JsonProperty("source")
//    RequestObjectEntry enrichProductEntry;
//
//    @JsonProperty("enrichers")
//    List<EnricherEntry> enricherResponseList;
//
//    public RequestObjectEntry getEnrichProductEntry() {
//        return enrichProductEntry;
//    }
//
//    public void setEnrichProductEntry(RequestObjectEntry enrichProductEntry) {
//        this.enrichProductEntry = enrichProductEntry;
//    }
//
//    public List<EnricherEntry> getEnricherResponseList() {
//        return enricherResponseList;
//    }
//
//    public void setEnricherResponseList(List<EnricherEntry> enricherResponseList) {
//        this.enricherResponseList = enricherResponseList;
//    }
//
//    @Override
//    public String toString() {
//        return "{" +
//                "\"source\":" + getEnrichProductEntry()  +
//                ",\"enrichers\":" + getEnricherResponseList() +
//
//                "}";
//    }
}
