package com.myntra.simple.client.entry;

/**
 * Created by pavankumar.at on 27/07/15.
 */

import com.myntra.commons.entries.AbstractEntry;

/***
 * This is how a custom entry is written
 */
public class GreetingEntry extends AbstractEntry {

    private String greeting;

    public String getGreeting() {
        return greeting;
    }

    public GreetingEntry (String greeting) {
        this.greeting = greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GreetingEntry that = (GreetingEntry) o;

        if (greeting != null ? !greeting.equals(that.greeting) : that.greeting != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return greeting != null ? greeting.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "GreetingEntry{" +
                "greeting='" + greeting + '\'' +
                '}';
    }
}
