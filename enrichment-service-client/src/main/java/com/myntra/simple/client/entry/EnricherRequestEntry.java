package com.myntra.simple.client.entry;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myntra.commons.entries.AbstractEntry;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize()
public class EnricherRequestEntry extends AbstractEntry {

	public EnricherRequestEntry() {
		// TODO Auto-generated constructor stub
	}
	
    @JsonProperty("id")
    String id;

    @JsonProperty("payload")
    JsonNode payload;

    public EnricherRequestEntry(String id, JsonNode payload) {
        this.id = id;
        this.payload = payload;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public JsonNode getPayload() {
        return payload;
    }

    public void setPayload(JsonNode payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + getId()  +
                ",\"payload\":" + getPayload() +
                "}";
    }
}
