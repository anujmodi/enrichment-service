package com.myntra.simple.client.entry;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.JsonNode;
import com.myntra.commons.entries.AbstractEntry;

@JsonRootName("data")
public class RequestObjectData extends AbstractEntry {

	@JsonProperty("clientId")
	private String clientId;

	@JsonProperty("entity")
	private JsonNode entity;

	public RequestObjectData() {
		// TODO Auto-generated constructor stub
	}

	public RequestObjectData(String id, JsonNode data) {
		this.clientId = id;
		this.entity = data;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String id) {
		this.clientId = id;
	}

	public JsonNode getEntity() {
		return entity;
	}

	public void setData(JsonNode data) {
		this.entity = data;
	}

	@Override
	public String toString() {
		return "{" + "\"clientId\":" + clientId + ",\"data\":" + entity + "}";
	}
}